import logging
import os

from flask import Flask, request
from flask_login import LoginManager
from logging.handlers import RotatingFileHandler
from flask_sslify import SSLify


# TODO: Celery, Redis, Flower, proper secret key

app = Flask(__name__)
app.secret_key = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
login = LoginManager(app)

from API.Users import Users as user_blueprint
from API.Recognition import Recognition as recognition_blueprint
from API.Training import Training as training_blueprint
from API.Maintenance import Maintenance as maintenance_blueprint
from API.Admin import Admin as admin_blueprint
from API.Errors_handlers import Errors as error_blueprint

if not app.debug:
    if not os.path.exists('logs'):
        os.mkdir('logs')
    file_handler = RotatingFileHandler('logs/PMIRecruitment.log', maxBytes=10240,
                                       backupCount=10)
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)

    app.logger.setLevel(logging.INFO)
    app.logger.info('PMIRecruitment startup')


@app.before_request
def log_request_info():
    # Works in debug only. Reason: there are some data which can leak like user-proposed password
    app.logger.debug('Headers: %s', request.headers)
    app.logger.debug('Body: %s', request.get_data())

context = ('web.crt', 'web.key')
# sslify = SSLify(app)  TODO: suppressed locally by PyCharm, fix this + tests for final deployment

app.register_blueprint(user_blueprint, url_prefix='/users')
app.register_blueprint(recognition_blueprint, url_prefix='/recognition')
app.register_blueprint(training_blueprint, url_prefix='/training')
app.register_blueprint(maintenance_blueprint, url_prefix='/maintenance')
app.register_blueprint(admin_blueprint, url_prefix='/admin')
app.register_blueprint(error_blueprint, url_prefix='/errors')
app.config.from_pyfile('config.py')


if __name__ == '__main__':
    # TODO: Add proper SSL/TSL certificate for production to secure proper HTTPS for data safety on...
    #  ...external API-client lane, especially passwords
    # Hide behind ngix, ngrok etc
    context = ('web.crt', 'web.key')
    app.run(debug=True, ssl_context=context)




