# PMI - RECRUITMENT TASK

Implement a secure CIFAR-10 Image Classification REST API in Python >= 3.5 with TensorFlow and Flask, or TensorFlow Serving. Create a public code repository with API source code, short README, Dockerfile, and a few tests. The Image Classification API should have at least two endpoints, for login and inference on a single picture, and should have simple logging and error handling.


The Deep Learning model has to be implemented as a Predictor class. This class should implement:

__init__ method - for loading the models/resources and running warmup


__predict__ method – the method that performs the inference


__warmup__ method - the optional warmup for the models
 

Feel free to use a pre-trained CIFAR-10 model and its TensorFlow implementation. The main objective of the task is to asses Python skills and coding style, project structure, REST API knowledge, and containerization using Docker.

NOTE: It is not recommended to put a lot of focus on improving the Image Classification model accuracy, as the task is about serving a model rather than achieving its best performance.

## Live Tests 

You can access my live local server :

```bash
http://0a1cba93.ngrok.io
```
Do not worry about strange url, it is provided by secure (and free!) tunnelling software https://ngrok.com/
Free version last couple of hours, the bad thing it can (and will) slowdown connection.

 [Image with example usage on non-local via http](https://i.imgur.com/Dn0dMR9.png)

## Security

```python
1. User authentication
2. Session storage
3. Hashed/Double salted passwords
4. (Partially) Https handling
5. Database credentials served as ENV

```

You need to log (& Register!) in before classifying. For user names choose any other than "TestUser", "SirNicolas", "SirVladyslav"

## Tests and error handling
Coverage is (unfortunately) symbolic

## Other
I pretty much time only yesterday evening + part of the night, and I got entangled into some librairies issues (as usual) :) I dedicated as much energy as I could. Not everything looks how i wanted but I have gave my best! Due to some ommited part of the task (Predictor class f.e) i added it too late so there is still some legacy code i coudnt really clean in time. Wanted to add admin panel + redis/celery/flowe, maybe next time :)

PS.I know you use pycharm thats i left over the /idea etc. Can quickly fix it if needed.

No matter the results please come back to me with feedback.

My feedback to recruiters: 
I enjoyed the f2f interview in terms of project and company showcase! 

What i dislike: 

-sluggish HR (waited almost 2 weeks for phone call, doesnt matter i applied by recommendation)

-Short period of time for task + term of it in the week. It is hard to reschedule things sometimes :(


## License & Contribution
:)