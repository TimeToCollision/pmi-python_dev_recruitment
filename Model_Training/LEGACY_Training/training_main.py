import pickle
import os
import tensorflow as tf

from Model_Training.LEGACY_Training.preprocessing import preprocess_and_save_data, normalize, one_hot_encode
from Model_Training.LEGACY_Training.CNN import conv_net, train_neural_network, print_stats, load_preprocess_training_batch
# from Model_Training.LEGACY_Training.exploration import display_stats

cifar10_dataset_folder_path = os.environ['cifar10_dataset_folder']
batch_meta = os.environ['batch_meta_path']
preprocess_path = os.environ['preprocess_path']
# batch_id = 3
# sample_id = 7000
# display_stats(cifar10_dataset_folder_path, batch_id, sample_id,batch_meta)

# Preprocess all the data and save it
preprocess_and_save_data(cifar10_dataset_folder_path, normalize, one_hot_encode, preprocess_path)

# load the saved dataset
valid_features, valid_labels = pickle.load(open(preprocess_path+'preprocess_validation.p', mode='rb'))

# Hyper parameters
epochs = 10
batch_size = 128
keep_probability = 0.7
learning_rate = 0.001

# Remove previous weights, bias, inputs, etc..
tf.compat.v1.reset_default_graph()

# Inputs
x = tf.compat.v1.placeholder(tf.float32, shape=(None, 32, 32, 3), name='input_x')
y = tf.compat.v1.placeholder(tf.float32, shape=(None, 10), name='output_y')
keep_prob = tf.compat.v1.placeholder(tf.float32, name='keep_prob')

# Build model
logits = conv_net(x, keep_prob)
model = tf.identity(logits, name='logits')  # Name logits Tensor, so that can be loaded from disk after training

# Loss and Optimizer
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Accuracy
correct_pred = tf.equal(tf.argmax(logits, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32), name='accuracy')

# LEGACY_Training Phase
save_model_path = os.environ['model_path']

with tf.Session() as sess:
    # Initializing the variables
    sess.run(tf.global_variables_initializer())

    # LEGACY_Training cycle
    for epoch in range(epochs):
        # Loop over all batches
        n_batches = 5
        for batch_i in range(1, n_batches + 1):
            for batch_features, batch_labels in load_preprocess_training_batch(batch_i, batch_size, preprocess_path):
                train_neural_network(sess, optimizer, keep_probability, batch_features, batch_labels, x, y, keep_prob)

            print('Epoch {:>2}, CIFAR-10 Batch {}:  '.format(epoch + 1, batch_i), end='')
            print_stats(sess, batch_features, batch_labels, cost, accuracy, x, y, keep_prob, valid_features, valid_labels)
    # Save Model_Training
    saver = tf.train.Saver()
    save_path = saver.save(sess, save_model_path)