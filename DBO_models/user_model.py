from flask_login import UserMixin
from random import randint
from app import login
from DB_connectors import Database

class User(UserMixin):
    def __init__(self, _name, _hash, _id=None):
        self.name = _name
        self.hashed = _hash
        if _id is None:
            self.id = randint(0, 1000)
        else:
            self.id = _id

    def return_credentials(self):
        return self.name, self.hashed


@login.user_loader
def load_user(id):
    name = None
    hashed = None
    _id = None
    db = Database.Database()
    name, hashed, _id = db.restore_user(id)
    if name is None:
        return None
    else:
        return User(name, hashed, _id)

