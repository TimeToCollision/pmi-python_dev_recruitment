from flask import Blueprint, url_for

Errors = Blueprint('errors', __name__)

from . import custom_http_handlers
