from flask import request, json
from app import app
from . import Errors
from werkzeug.exceptions import HTTPException

# TODO: more sophisticated error handlers

@Errors.errorhandler(404)
def page_not_found(error):
    app.logger.error('Page not found: %s', (request.path))


@Errors.errorhandler(401)
def page_not_found(error):
    app.logger.error('401 Unauthorized', (request.path))


@Errors.errorhandler(412)
def handle_bad_request(e):
    app.logger.error('412 Precondition Failed', (request.path))
    return 'Precondition Failed!', 400


@Errors.errorhandler(Exception)
def unhandled_exception(e):
    app.logger.error('Unhandled Exception: %s', (e))

@Errors.errorhandler(HTTPException)
def handle_exception(e):
    response = e.get_response()
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    app.logger.error('HTTP Exception: %s', (e))
    return response