import psycopg2
import os
from flask import current_app
from app import app
from DBO_models import user_model


class Database():
    # TODO: Add string builder

    def __init__(self):
        db = current_app.config['DB_NAME']
        host = current_app.config['DB_HOST']
        username = current_app.config['DB_USER']
        password = os.environ['DB_PASS']
        self.con = psycopg2.connect("dbname='{}' user='{}' password='{}' host='{}'".format(db, username, password, host))
        self.cur = self.con.cursor()

    def __del__(self):
        self.cur.close()
        self.con.close()

    def availability_test(self):
        msg = None
        try:
            self.cur.execute("SELECT * FROM {} ORDER BY {} DESC LIMIT 1;".format("\"Pinger\"", "\"Pinger\"", ))
            msg = self.cur.fetchall()
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            app.logger.info("Error while connecting to PostgreSQL", error)
        if msg is not None:
            return msg[0][0]
        else:
            return "error"

    def restore_salt(self):
        msg = None
        try:
            self.cur.execute("SELECT * FROM {};".format("\"salt\""))
            msg = self.cur.fetchall()
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            app.logger.info("Error while connecting to PostgreSQL", error)
        if msg is not None:
            return msg[0][1]
        else:
            return "error"

    def store_user(self, user_object):

        name, hashed = user_object.return_credentials()
        try:
            self.cur.execute("INSERT INTO {} ({}) VALUES ({},{});".format("\"Users\"", "\"name\", \"hash\"", '\'' + str(name) + '\'', '\'' + str(hashed) + '\''))
            self.con.commit()
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            app.logger.info("Error while connecting to PostgreSQL", error)

    def restore_user_id(self, name):
        msg = None
        try:
            self.cur.execute("SELECT {} FROM {} WHERE {} = {};".format('id',"\"Users\"", 'name', "\'" + name + "\'"))
            msg = self.cur.fetchall()
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            app.logger.info("Error while connecting to PostgreSQL", error)
        if msg is not None:
            return msg[0][0]
        else:
            return "error"

    def restore_user(self, id):
        msg = None
        try:
            self.cur.execute("SELECT {} FROM {} WHERE {} = {};".format('*',"\"Users\"", 'id', id ))
            msg = self.cur.fetchall()
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            app.logger.info("Error while connecting to PostgreSQL", error)
        if msg is not None:
            return msg[0]
        else:
            return "error"

    def restore_user_by_name(self, name):
        msg = None
        try:
            self.cur.execute("SELECT {} FROM {} WHERE {} = {};".format('*',"\"Users\"", 'name', "\'" + name + "\'"))
            msg = self.cur.fetchall()
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            app.logger.info("Error while connecting to PostgreSQL", error)
        if msg is not None:
            return msg[0]
        else:
            return None
