from flask import Blueprint, url_for

Training = Blueprint('training', __name__)

# TODO:
#  remote re-training procedure launch
#  dataset substitution (or addition)
#  model versioning/distribution/storage
#  asynchronous handler?
#  stats?
