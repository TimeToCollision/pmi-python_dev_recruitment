from flask import request,  json, Response
from flask import current_app
from PIL import Image
from . import Recognition
from Recognition.helpers import model_handling, image_handling, Predictor
from flask_login import login_required



@Recognition.route('/predictor', methods=['PUT'])
@login_required
def predictor_test():

    img = Image.open(request.files['image'])
    img_path = current_app.config['TMP_IMG_PATH']+'tmp.png'
    img.save(img_path)

    predictor = Predictor.Predictor(img_path, current_app.config['MODEL_PATH'])
    most_probable = predictor.predict()
    dict_tmp = {"Most_Probable": most_probable}

    return Response(json.dumps(dict_tmp), mimetype='application/json')



# LEGACY - i forgot about "Predictor class" requirement in the task description
@Recognition.route('/classify_test', methods=['PUT'])
@login_required
def classify_test():

    img = Image.open(request.files['image'])
    img_path = current_app.config['TMP_IMG_PATH']+'tmp.png'
    img.save(img_path)
    image_local = image_handling.load_image(img_path)

    result = model_handling.classify(image_local, current_app.config['MODEL_PATH'])
    most_probable = model_handling.most_probable_class(result)
    dict_tmp = {"Most_Probable": most_probable}

    return Response(json.dumps(dict_tmp), mimetype='application/json')


