from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from PIL import Image


def load_image(filename):
    # load the image
    img = load_img(filename, target_size=(32, 32))
    # convert to array
    img = img_to_array(img)
    # reshape into a single sample with 3 channels
    img = img.reshape(1, 32, 32, 3)
    # prepare pixel data
    img = img.astype('float32')
    img = img / 255.0
    return img


def resize_image(filename):
    img = Image.open(filename)
    new_img = img.resize((32,32))
    return new_img

