import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler, normalize


def load_label_names():
    return ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']


def return_prediction(label_names, index):
    return label_names[index]


def classify(image, model_path):
    model = tf.keras.models.load_model(model_path)
    result = model.predict_classes(image)

    # result_prob = model.predict(image)
    # normalized = normalize_1d(result_prob[0])

    return result


def normalize_1d(array):

    scaler = MinMaxScaler()
    scaled_x = scaler.fit_transform(array)
    normalized = normalize(scaled_x, norm='l1', axis=1, copy=True)

    return normalized


def most_probable_class(results):
    most_probable = return_prediction(load_label_names(), results[0])
    return most_probable
