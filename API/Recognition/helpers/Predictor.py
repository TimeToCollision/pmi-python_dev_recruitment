import tensorflow as tf
from Recognition.helpers import image_handling, model_handling


class Predictor:
    def __init__(self, image, model_path):
        self.image = image_handling.load_image(image)
        self.model = tf.keras.models.load_model(model_path)

    def predict(self):
        result = self.model.predict_classes(self.image)
        return model_handling.most_probable_class(result)

    def _warmup(self):
        pass
