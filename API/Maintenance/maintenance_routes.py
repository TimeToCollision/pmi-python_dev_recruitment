from . import Maintenance
from flask import request
from DB_connectors import Database
from app import app


@Maintenance.route('/database_check', methods=['GET'])
def database_check():

    db = Database.Database()
    return db.availability_test()


@Maintenance.route('/app_check', methods=['GET'])
def app_check():
    return 'I am fine!'

@Maintenance.before_request
def log_request_info():
    app.logger.debug('Headers: %s', request.headers)
    app.logger.debug('Body: %s', request.get_data())