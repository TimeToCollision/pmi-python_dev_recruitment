from flask import Blueprint

Maintenance = Blueprint('maintenance', __name__)

from . import maintenance_routes