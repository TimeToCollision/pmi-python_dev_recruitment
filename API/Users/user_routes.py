from Users.Helpers import hashing
from . import Users
from app import app
from flask import request
from DB_connectors import Database
from DBO_models import user_model
from flask_login import login_user, logout_user, current_user


@Users.route('/register', methods=['POST'])
def register():

    name = request.headers.get("name")
    password = request.headers.get("password")
    db = Database.Database()
    name, password, _id = db.restore_user_by_name(name)
    if name is None:

        salt = db.restore_salt()
        hashed = hashing.hash_password(password, salt)
        created_user = user_model.User(name, hashed)
        db.store_user(created_user)

        app.logger.info('%s registered succesfully', name)

        return 'Created account for ' + name

    app.logger.info('%s name exists already', name)
    return 'Such name exists already!'


@Users.route('/login', methods=['GET'])
def login():
    provided_name = request.headers.get("name")
    provided_password = request.headers.get("password")

    if current_user.is_authenticated:
        app.logger.info('%s already authenticated', provided_name)
        return 'Already authenticated'

    db = Database.Database()
    name, password, _id = db.restore_user_by_name(provided_name)

    if name is None:
        app.logger.info('There is no such user as %s', name)
        return 'There is no such user!'

    salt = db.restore_salt()

    if hashing.check_password(password, provided_password, salt) is True:
        tmp_user = user_model.User(name, password, _id)
        login_user(tmp_user)
        app.logger.info('User %s logged in', name)
        return 'Logged in'
    else:
        app.logger.info('Unvalid credentials for %s', name)
        return 'Unvalid credentials!'


@Users.route('/logout')
def logout():
    name = current_user.name
    logout_user()
    app.logger.info('%s logged out', name)
    return 'Succesfully logged out'