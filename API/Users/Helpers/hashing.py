from werkzeug.security import generate_password_hash, check_password_hash


def hash_password(password, salt):
    hashed = generate_password_hash(password+salt)
    return hashed


def check_password(hashed, password, salt):
    test = password + salt
    return check_password_hash(hashed, test)


