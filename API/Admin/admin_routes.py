from . import Admin
from DB_connectors import Database


@Admin.route('/check_id', methods=['GET'])
def check_id():
    db = Database.Database()
    id = db.restore_user_id('TestUser')
    return 'checked id ' + id


@Admin.route('/restore_user', methods=['GET'])
def restore_user():
    db = Database.Database()
    id = db.restore_user(1)
    print(id)
    return 'checked id '
