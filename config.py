batch_meta_path = "D:\Work\PMIRecruitmentTask\Resources\DataSet\cifar-10-python\cifar-10-batches-py\batches.meta"
cifar10_dataset_folder = "D:\Work\PMIRecruitmentTask\Resources\DataSet\cifar-10-python\cifar-10-batches-py"


# PATHS:
MODEL_PATH = 'Resources/Model/Cifar10_classification.h5'
TMP_IMG_PATH = 'Resources/Images/Temporary/'


# DATABASE:
DB_NAME = 'postgres'
DB_USER = 'postgres'
DB_PORT = '5432'
DB_HOST = '127.0.0.1'