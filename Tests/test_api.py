import os
import unittest
from unittest import mock
import requests, json

from app import app

class BasicTests(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################

    # executed prior to each test
    def setUp(self):
        self.app = app.test_client()

    # executed after each test
    def tearDown(self):
        pass

    ###############
    #### tests ####
    ###############

    # TODO: better (actual) coverage

    def test_health(self):
        response = self.app.get('/maintenance/app_check', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data , b'I am fine!')


    def test_db_health(self):
        with mock.patch.dict('os.environ', {'DB_PASS': '13jockper'}):
            response = self.app.get('/maintenance/database_check', follow_redirects=True)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.data , b'I am still here my friend!')

    def test_registration_bad_method(self):
        response = self.app.get('users/register', follow_redirects=True)
        self.assertEqual(response.status_code, 405)

    def test_registration_name_exists(self):
        with mock.patch.dict('os.environ', {'DB_PASS': '13jockper'}):
            headers = {'name': 'Dawidos', 'password': 'PMIa'}
            response = self.app.post('users/register', headers=headers , follow_redirects=True)
            self.assertEqual(response.data, b'Such name exists already!')

    def test_login_negative(self):
        with mock.patch.dict('os.environ', {'DB_PASS': '13jockper'}):
            headers = {'name': 'Dawidos', 'password': 'PMIa'}
            response = self.app.get('users/login', headers=headers , follow_redirects=True)
            self.assertEqual(response.data, b'Unvalid credentials!')

    def test_login(self):
        with mock.patch.dict('os.environ', {'DB_PASS': '13jockper'}):
            headers = {'name': 'Dawidos', 'password': 'PMI'}
            response = self.app.get('users/login', headers=headers , follow_redirects=True)
            self.assertEqual(response.data, b'Logged in')

    def test_recognitionv2(self):
        with mock.patch.dict('os.environ', {'DB_PASS': '13jockper'}):
            headers = {'name': 'Dawidos', 'password': 'PMI'}
            self.app.get('users/login', headers=headers , follow_redirects=True)
            with open("../Resources/Images/sample_image-1.png", 'rb') as img:
                name_img = os.path.basename("../Resources/Images/sample_image-1.png")
                files = {'image': (name_img, img, 'multipart/form-data', {'Expires': '0'})}
                with requests.Session() as s:
                    r = s.put('http://127.0.0.1:5000/recognition/classify-single', files=files)
                    self.assertEqual(r.json(), b'{"Most_Probable": "horse"}')
    #                TODO: Fix above

    def test_logout(self):
        with mock.patch.dict('os.environ', {'DB_PASS': '13jockper'}):
            headers = {'name': 'Dawidos', 'password': 'PMI'}
            self.app.get('users/login', headers=headers , follow_redirects=True)
            response = self.app.get('users/logout', headers=headers, follow_redirects=True)
            self.assertEqual(response.data, b'Succesfully logged out')



if __name__ == "__main__":
    unittest.main()