import unittest
import tensorflow as tf
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from Model_Training.Simplified_model.Model_tryout import load_image

class BasicTests(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################

    # executed prior to each test
    def setUp(self):
        pass

    # executed after each test
    def tearDown(self):
        pass

    ###############
    #### tests ####
    ###############

    # TODO: better (actual) coverage

    def test_model_sample_classification(self):
        # load the image
        img = load_image("../Resources/Images/sample_image-1.png")
        # load model
        model = tf.keras.models.load_model("../Resources/Model/Cifar10_classification.h5")
        # predict the class
        result = model.predict_classes(img)
        self.assertEqual(result[0], 4)



if __name__ == "__main__":
    unittest.main()